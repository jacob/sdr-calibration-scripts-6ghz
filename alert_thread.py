import threading
import time

class AlertThread(threading.Thread):
    def run(self):
        self.running = True
        while self.running:
            print("\a", end="", flush=True)
            time.sleep(2)
